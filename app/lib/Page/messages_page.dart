import 'package:flutter/material.dart';

class MessagesPage extends StatefulWidget {
  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Tooltip(
          message: 'Message Received: 11/09/2020, 1:00PM',
          child: Card(
            child: Container(
              padding: EdgeInsets.all(5.0),
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    'General Kenobi',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text('Hello there.'),
                  trailing: Icon(Icons.mark_chat_unread_outlined),
                ),
              ),
            ),
          ),
        ),
        Tooltip(
          message: 'Message Received: 11/07/2020, 6:32PM',
          child: Card(
            child: Container(
              padding: EdgeInsets.all(5.0),
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    'Mom',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text('Are you coming over for dinner?'),
                  trailing: Icon(Icons.mark_chat_unread_outlined),
                ),
              ),
            ),
          ),
        ),
        Tooltip(
          message: 'Message Read: 11/07/2020, 3:15PM',
          child: Card(
            child: Container(
              padding: EdgeInsets.all(5.0),
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    '(066) 555-1212',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle:
                      Text('Hey I found your ID card on the floor, call me!'),
                  trailing: Icon(Icons.mark_chat_read_outlined),
                ),
              ),
            ),
          ),
        ),
        Tooltip(
          message: 'Message Sent: 11/07/2020, 3:15PM',
          child: Card(
            child: Container(
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    'Ann',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text('Thanks for the bike, Ill return it soon!'),
                  trailing: Icon(Icons.outbond_outlined),
                ),
              ),
            ),
          ),
        ),
        Tooltip(
          message: 'Message Read: 11/06/2020, 6:57AM',
          child: Card(
            child: Container(
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    'Satan',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text('Captain Teemo, on duty!'),
                  trailing: Icon(Icons.mark_chat_read_outlined),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
