import 'package:app/Classes/contact.dart';
import 'package:flutter/material.dart';
import 'package:app/Page/contacts_page.dart';
import 'package:app/Page/calls_page.dart';
import 'package:app/Page/messages_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  List<Widget> _tabs = [
    Tab(icon: Icon(Icons.message)),
    Tab(icon: Icon(Icons.call)),
    Tab(icon: Icon(Icons.person)),
  ];

  List<Contact> _contacts = [
    Contact("Mom", "", "(055) 555-5555", "mom@test.com"),
    Contact("General", "Kenobi", "(043) 444-4444", "gk@test.com"),
    Contact("Ann", "", "(+33)868686868", "ann@test.com"),
    Contact("Satan", "", "(666) 666-6666", "satan@test.com"),
  ];

  @override
  initState() {
    _controller = TabController(length: _tabs.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          bottom: TabBar(
            controller: _controller,
            tabs: _tabs,
          ),
        ),
        body: TabBarView(
          controller: _controller,
          children: [
            MessagesPage(),
            CallsPage(),
            ContactsPage(_contacts),
          ],
        ));
  }
}
