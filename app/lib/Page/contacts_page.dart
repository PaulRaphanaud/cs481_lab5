import 'package:app/Classes/contact.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/Widgets/add_contact_sheet.dart';
import 'package:flutter_slidable/flutter_slidable.dart';


class ContactsPage extends StatefulWidget {
  ContactsPage(this.contacts, {Key key}) : super(key: key);

  final List<Contact> contacts;

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {

  SlidableController slidableController;
  Animation<double> _rotationAnimation;
  Color _fabColor = Colors.blue;

  @override
  void initState() {
    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );
    super.initState();
  }

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }

  void onAddContact(Contact newContact) {
    setState(() {
      widget.contacts.add(newContact);
    });
  }

  onAddButtonPressed() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        builder: (BuildContext context) {
          return AddContactSheet(onAddContact);
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: onAddButtonPressed,
        child: Icon(Icons.person_add_alt_1),
      ),
      body: ListView.builder(
          itemCount: widget.contacts.length,
          itemBuilder: (context, idx) {
            return Tooltip(
              message: "Swipe to delete contact",
              child: Slidable(
                controller: slidableController,
                key: Key(widget.contacts[idx].phone_number),
                actionPane: SlidableScrollActionPane(),
                actionExtentRatio: 0.25,
                secondaryActions: <Widget>[
                  Card(
                    child: IconSlideAction(
                      caption: 'Delete',
                      color: Colors.redAccent,
                      icon: Icons.delete,
                      onTap: () {
                        setState(() {
                          widget.contacts.removeAt(idx);
                        });
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Contact Deleted")));
                      }
                    ),
                  ),
                ],
                child: Card(
                  child: ListTile(
                    leading: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            colors: [
                              Colors.redAccent,
                              Colors.orangeAccent
                            ],
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                          )
                      ),
                      child: CircleAvatar(
                        child: Text(widget.contacts[idx].initials()),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    title: Text(widget.contacts[idx].first_name + ' ' + widget.contacts[idx].last_name),
                    subtitle: Text(widget.contacts[idx].phone_number),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }
}
