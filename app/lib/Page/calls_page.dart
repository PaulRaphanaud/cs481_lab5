import 'package:app/Classes/call.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CallsPage extends StatefulWidget {
  @override
  _CallsPageState createState() => _CallsPageState();
}

class _CallsPageState extends State<CallsPage> {
  static List<Call> _calls = [
    Call("Spam Suspicion", "(+33)957241769", "8:30 AM", "Not recorded",
        Icon(Icons.call_missed, size: 14, color: Colors.red),
        CircleAvatar(
          child: Icon(Icons.dangerous, color: Colors.red),
          backgroundColor: Colors.grey[350],
        )
    ),
    Call("Mom", "Mobile", "5:08 PM", "Not recorded",
        Icon(Icons.call_received_outlined, size: 14, color: Colors.green,),
        CircleAvatar(
          child: Text("M"),
          backgroundColor: Colors.orangeAccent,
        )
    ),
    Call("General Kenobi", "Mobile", "8:15 PM", "Not recorded",
        Icon(Icons.call_missed_rounded, size: 14, color: Colors.red), CircleAvatar(
          child: Text("GK"),
          backgroundColor: Colors.orangeAccent,
        )
    ),
    Call("General Kenobi", "Mobile", "8:23 PM", "Not recorded",
        Icon(Icons.call_made_rounded, size: 14, color: Colors.orangeAccent), CircleAvatar(
          child: Text("GK"),
          backgroundColor: Colors.orangeAccent,
        )
    ),
  ];

  void _copyPhoneNumber(String phoneNumber) {
    Clipboard.setData(ClipboardData(text: phoneNumber)).then((result) {
      final snackBar = SnackBar(
        content: Text('Phone Number Copied to Clipboard'),
        action: SnackBarAction(
          label: 'Ok',
          onPressed: () {},
        ),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _calls.length,
      itemBuilder: (context, index) {
        return ListTile(
          leading: _calls[index].avatar,
          title: Text(_calls[index].name),
          subtitle: Row(
            children: [
              _calls[index].status_icon,
              Text(_calls[index].phone_number)
            ],
          ),
          trailing: Text(_calls[index].date),
          onLongPress: () => _copyPhoneNumber(_calls[index].phone_number),
        );
      },
    );
  }
}
