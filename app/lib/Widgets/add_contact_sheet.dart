import 'package:app/Classes/contact.dart';
import 'package:flutter/material.dart';

class AddContactSheet extends StatefulWidget {
  AddContactSheet(this.onAddContact, {Key key}) : super(key: key);

  final Function onAddContact;

  @override
  _AddContactSheetState createState() => _AddContactSheetState();
}

class _AddContactSheetState extends State<AddContactSheet> {

  TextEditingController fn_controller = TextEditingController();
  TextEditingController ln_controller = TextEditingController();
  TextEditingController email_controller = TextEditingController();
  TextEditingController pn_controller = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                SizedBox(width: MediaQuery.of(context).size.width /2.75),
                Text("New Contact", style: TextStyle(fontSize: 18)),
                SizedBox(width: 70),
                GestureDetector(
                  onTap: () {
                    widget.onAddContact(Contact(
                        fn_controller.text.toString(),
                        ln_controller.text.toString(),
                        pn_controller.text.toString(),
                        email_controller.text.toString()
                      )
                    );
                    Navigator.pop(context);
                  },
                  child: Text("Done", style: TextStyle(
                      color: Color.fromRGBO(35, 172, 238, 1),
                      fontSize: 16,
                  )
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text("First Name :"),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextField(
                          controller: fn_controller,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text("Last Name :"),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextField(
                          controller: ln_controller,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text("Phone Number :"),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextField(
                          controller: pn_controller,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text("Email :"),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextField(
                          controller: email_controller,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}