class Contact {
  String first_name;
  String last_name;
  String phone_number;
  String email;

  Contact(this.first_name, this.last_name, this.phone_number, this.email);

  String initials() {
    if (this.first_name.isNotEmpty && this.last_name.isNotEmpty)
      return (this.first_name[0] + this.last_name[0]).toUpperCase();
    else if (this.first_name.isNotEmpty && this.last_name.isEmpty)
      return this.first_name[0].toUpperCase();
    else if (this.first_name.isEmpty && this.last_name.isNotEmpty)
      return this.last_name[0].toUpperCase();
    else
      return "";
  }
}