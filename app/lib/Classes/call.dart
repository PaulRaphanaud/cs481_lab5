import 'package:flutter/material.dart';

class Call {
  String name;
  String phone_number;
  String date;
  String status;
  Icon status_icon;
  CircleAvatar avatar;

  Call(this.name, this.phone_number, this.date, this.status, this.status_icon, this.avatar);

}